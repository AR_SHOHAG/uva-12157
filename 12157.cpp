#include <cstdio>

int main()
{
    int T, N, num, mile, juice;
    scanf("%d", &T);

    for (int t = 1; t <= T; ++t)
    {
        scanf("%d", &N);
        mile = juice = 0;
        while (N--)
        {
            scanf("%d", &num);
            mile += (num / 30) * 10 + 10;
            juice += (num / 60) * 15 + 15;
        }

        if (mile < juice)
            printf("Case %d: Mile %d\n",t, mile);
        if (mile == juice)
            printf("Case %d: Mile Juice %d\n", t, mile);

        if (juice < mile)
            printf("Case %d: Juice %d\n",t, juice);
    }
    return 0;
}
